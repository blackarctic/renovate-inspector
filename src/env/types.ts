export type InjectedEnv = {
  RENOVATE_CONFIG_FILE?: string;
  RENOVATE_TOKEN?: string;
  RENOVATE_INSPECTOR_LABEL?: string;
  RENOVATE_INSPECTOR_ADDITIONAL_MESSAGE?: string;
  RENOVATE_INSPECTOR_MR_LIMIT?: string;
  SLACK_WEBHOOK_URL?: string;
};

export type Env = {
  RENOVATE_TOKEN: string;
  ENDPOINT: string;
  DRY_RUN: boolean;
  PLATFORM: string;
  REPOS: { repo: string; channel: string }[];
  RENOVATE_INSPECTOR_LABEL: string;
  RENOVATE_INSPECTOR_ADDITIONAL_MESSAGE?: string;
  RENOVATE_INSPECTOR_MR_LIMIT: number;
  SLACK_WEBHOOK_URL?: string;
};
