export type Author = {
  id: number;
  name: string;
  username: string;
  state: string;
  avatar_url: string;
  web_url: string;
};

export type Pipeline = {
  id: number;
  sha: string;
  ref: string;
  status: string;
  web_url: string;
};

export type ApprovalState = {
  approval_rules_overwritten: boolean;
  rules: Array<{
    id: number;
    name: string;
    rule_type: string;
    approvals_required: number;
    approved_by: [
      {
        id: number;
        name: string;
        username: string;
        state: string;
        avatar_url: string;
        web_url: string;
      }
    ];
    approved: boolean;
  }>;
};

export type ApprovalRule = {
  id: number;
  name: string;
  rule_type: string;
  eligible_approvers: {
    id: number;
    name: string;
    username: string;
    state: string;
    avatar_url: string;
    web_url: string;
  }[];
  approvals_required: number;
  source_rule: unknown;
  users: {
    id: number;
    name: string;
    username: string;
    state: string;
    avatar_url: string;
    web_url: string;
  }[];
  groups: {
    id: number;
    name: string;
    path: string;
    description: string;
    visibility: string;
    lfs_enabled: boolean;
    avatar_url: unknown;
    web_url: string;
    request_access_enabled: boolean;
    full_name: string;
    full_path: string;
    parent_id: unknown;
    ldap_cn: unknown;
    ldap_access: unknown;
  }[];
  contains_hidden_groups: boolean;
  overridden: boolean;
};

export type Mr = {
  id: number;
  iid: number;
  project_id: number;
  title: string;
  description: string;
  state: string;
  created_at: string;
  updated_at: string;
  author: Author;
  labels: string[];
  work_in_progress: boolean;
  merge_status: string;
  web_url: string;
  changes_count: string;
  pipeline: Pipeline;
  head_pipeline: Pipeline;
  has_conflicts: boolean;
  merge_error: string;
  blocking_discussions_resolved: boolean;
};
