import { env } from "../env";
import { sendFormattedMessage } from "../requests/slack/sendMessage";

export const sendSlackMessage = async ({
  type,
  repo,
  amount,
  channel,
}: {
  type: "approaching" | "exceeded";
  repo: string;
  amount: number;
  channel: string;
}): Promise<void> => {
  if (env.SLACK_WEBHOOK_URL) {
    let fallback: string;
    let pretext: string;
    let color: string;
    let firstMessage: string;
    let secondMessage: string;

    switch (type) {
      case "approaching":
        fallback = `🟡 Approaching RenovateBot Limit`;
        pretext = `🟡 *Approaching RenovateBot Limit*`;
        color = `#FFD800`;
        firstMessage = `The repo *${repo}* is close to exceeding the limit of open RenovateBot MRs (*${amount}/${env.RENOVATE_INSPECTOR_MR_LIMIT}*).`;
        secondMessage = `If this happens, RenovateBot will *skip running on this repo* until it is no longer above the limit.`;
        break;
      case "exceeded":
        fallback = `🔴 Exceeded RenovateBot Limit`;
        pretext = `🔴 *Exceeded RenovateBot Limit*`;
        color = `#D71717`;
        firstMessage = `The repo *${repo}* has exceeded the limit of open RenovateBot MRs (*${amount}/${env.RENOVATE_INSPECTOR_MR_LIMIT}*).`;
        secondMessage = `RenovateBot will *skip running on this repo* until it is no longer above the limit.`;
        break;
      default:
        return;
    }

    ///////////////////////////////////

    console.log(
      `Issuing "${type}" warning to "${channel}" about "${repo}" (${amount}/${env.RENOVATE_INSPECTOR_MR_LIMIT})`
    );

    const additionalMessage = env.RENOVATE_INSPECTOR_ADDITIONAL_MESSAGE
      ? env.RENOVATE_INSPECTOR_ADDITIONAL_MESSAGE
      : undefined;
    const messageFields = [
      {
        value: firstMessage,
        short: false,
      },
      {
        value: secondMessage,
        short: false,
      },
    ];
    if (additionalMessage) {
      messageFields.push({
        value: additionalMessage,
        short: false,
      });
    }
    const message = [
      {
        fallback,
        pretext,
        color,
        fields: messageFields,
      },
    ];

    try {
      await sendFormattedMessage({
        message,
        channel,
      });
    } catch (error) {
      console.log(error);
      // Do nothing. It's ok. We will simply keep going.
    }
  }
};
