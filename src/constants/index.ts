import path from "path";

export const outDir = path.join(__dirname, "..");
export const outFile = path.join(outDir, "results.json");
