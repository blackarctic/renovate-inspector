# renovate-inspector

A small script meant to run alongside RenovateBot to inspect & report the amount of open RenovateBot MRs in Gitlab.

## How To Use

1. Install `renovate-inspector`

```bash
yarn add renovate-inspector
# or
npm install renovate-inspector
```

2. Ensure you have a proper RenovateBot environment.

   - Be sure you have `RENOVATE_CONFIG_FILE` available in your env.
   - Be sure you have `RENOVATE_TOKEN` available in your env.

3. If you wish to do so, set up any optional environment variables.

   - In order to send Slack messages, set your Slack Webhook URL with `SLACK_WEBHOOK_URL="https://hooks.slack.com/services/<my slack webhook url path>"`. This defaults to `undefined` and will send no Slack messages.
   - Set your target MR label with `RENOVATE_INSPECTOR_LABEL="<my renovatebot label>"`. This defaults to "RenovateBot".
   - Set your RenovateBot open MR limit with `RENOVATE_INSPECTOR_MR_LIMIT="<number of open mrs allowed>"`. This defaults to `10`.
   - Set an additional message to be included at the end of Slack messages with `RENOVATE_INSPECTOR_ADDITIONAL_MESSAGE="<additional message>"`. This defaults to `undefined` and will display no additional message. This requires `SLACK_WEBHOOK_URL` to be set.

4. Wherever you run `./node_modules/.bin/renovate`, run `./node_modules/.bin/renovate-inspector` before it.

5. Update your `renovate.config.js` to be compatible with `renovate-inspector`:

```js
// renovate.config.js

let reposToBeSkipped = [];
try {
  const renovateInspectorResults = require("renovate-inspector/lib/results.json");
  reposToBeSkipped = renovateInspectorResults.reposToBeSkipped;
} catch (error) {
  // Do nothing. It's ok if renovate-inspector results are not present.
  // This will happen when renovate-inspector is running and using this file.
}

const repos = [
  { repo: "my-user/my-repo-1", channel: "@my-slack-username" },
  { repo: "my-user/my-repo-2", channel: "#my-slack-channel" },
].filter((x) => !reposToBeSkipped.includes(x.repo));
const repositories = repos.map((x) => x.repo);

module.exports = {
  ...
  platform: "gitlab",
  repos,
  repositories,
};
```

## Contributing

Repo: <https://gitlab.com/blackarctic/renovate-inspector>

Be sure to run `yarn run check` to ensure proper formatting and linting.

## Authors

Created by Nick Galloway
